package id.sch.smktelkom_mlg.profilenavigation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        bottom_nav_view?.let { bottomNavigationView ->
            NavigationUI.setupWithNavController(bottom_nav_view, navController)
        }
        navController.addOnNavigatedListener { controller, destination ->
            if (destination.id == R.id.fragment_photos || destination.id == R.id.fragment_like || destination.id == R.id.fragment_camera) {
                bottom_nav_view.let {
                    bottom_nav_view.visibility = View.GONE
                }
            } else {
                bottom_nav_view.visibility = View.VISIBLE
            }
        }
    }
}
